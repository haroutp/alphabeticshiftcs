﻿using System;

namespace AlphabeticShift
{
    class Program
    {
        static void Main(string[] args)
        {
            System.Console.WriteLine(alphabeticShift("crazy"));
            System.Console.WriteLine(alphabeticShift("z"));

            System.Console.WriteLine(alphabeticShift("aaaabbbccd"));
            System.Console.WriteLine(alphabeticShift("fuzzy"));
            System.Console.WriteLine(alphabeticShift("codesignal"));
            System.Console.WriteLine(alphabeticShift("jackodinharout"));

        }

        static string alphabeticShift(string inputString) {
            char[] sendBack = inputString.ToCharArray();

            
            for(int i = 0; i < inputString.Length; i++){
                if(inputString[i] == 'z'){
                    sendBack[i] = 'a';
                    continue;
                }
                sendBack[i] = (char)((int)inputString[i] + 1);
            }
            return (string.Concat(sendBack));
        }


    }
}
